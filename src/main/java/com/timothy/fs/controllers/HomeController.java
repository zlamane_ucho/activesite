package com.timothy.fs.controllers;

import com.timothy.fs.dto.UserDto;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

@Controller
public class HomeController {

    @RequestMapping({"/", ""})
    public String showIndex() {
        return "index";
    }

    @GetMapping("/registerWithAjax")
    public String showRegisterWithAjax() {
        return "registerWithAjax";
    }

	@GetMapping("/addProducts")
	public String showAddProducts() {
		return "addProducts";
	}

    @GetMapping("/register")
    public String showRegister(ModelMap map) {
        map.put("userDto", new UserDto());
        return "register";
    }
}

