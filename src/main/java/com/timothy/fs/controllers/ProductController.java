package com.timothy.fs.controllers;


import com.timothy.fs.dto.ProductDto;
import com.timothy.fs.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService productService;

	@PostMapping("/add")
	public String addProductToDB(@RequestBody ProductDto product){
		productService.addProduct(product);
		return product.toString();
	}

}
