package com.timothy.fs.controllers;


import com.timothy.fs.dto.UserDto;
import com.timothy.fs.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

@RestController
@RequestMapping("/user")
public class UserController {

	@Autowired
	private UserService userService;

	@PostMapping("/register")
	public RedirectView addUserToDB(@ModelAttribute UserDto userDto){
		userService.addUser(userDto);
		//TODO: Redirect to LOGIN PAGE? if user was registered
		return new RedirectView("/register");
	}
}
