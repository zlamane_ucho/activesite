package com.timothy.fs.dao;

import com.timothy.fs.dto.ProductDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IProductDao extends CrudRepository<ProductDto, Integer> {

}
