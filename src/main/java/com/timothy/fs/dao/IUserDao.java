package com.timothy.fs.dao;

import com.timothy.fs.dto.UserDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IUserDao extends CrudRepository<UserDto, Integer> {

}
