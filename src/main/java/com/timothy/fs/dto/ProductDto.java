package com.timothy.fs.dto;

import javax.persistence.*;

@Entity
@Table(name = "PRODUCTS")
public class ProductDto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer id;
	private String name;
	private int protein;
	private int carbs;
	private int fat;

	public ProductDto() {
	}

	public ProductDto(String name, int protein, int carbs, int fat) {
		this.name = name;
		this.protein = protein;
		this.carbs = carbs;
		this.fat = fat;
	}

	@Override
	public String toString() {
		return "ProductDto{" +
				"id=" + id +
				", name='" + name + '\'' +
				", protein=" + protein +
				", carbs=" + carbs +
				", fat=" + fat +
				'}';
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getProtein() {
		return protein;
	}

	public void setProtein(int protein) {
		this.protein = protein;
	}

	public int getCarbs() {
		return carbs;
	}

	public void setCarbs(int carbs) {
		this.carbs = carbs;
	}

	public int getFat() {
		return fat;
	}

	public void setFat(int fat) {
		this.fat = fat;
	}
}
