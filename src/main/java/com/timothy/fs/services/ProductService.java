package com.timothy.fs.services;


import com.timothy.fs.dao.IProductDao;
import com.timothy.fs.dto.ProductDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService{

	@Autowired
	private IProductDao iProductDao;

	public void addProduct(ProductDto product){ iProductDao.save(product); }

}
