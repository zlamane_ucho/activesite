package com.timothy.fs.services;

import com.timothy.fs.dao.IUserDao;
import com.timothy.fs.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

	@Autowired
	private IUserDao iUserDao;

	public void addUser(UserDto userDto){
		iUserDao.save(userDto);
	}
}
