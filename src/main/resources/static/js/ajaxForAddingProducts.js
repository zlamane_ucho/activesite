$(document).ready(function () {

    $("#submitProduct").click(function (event) {
        event.preventDefault();
        var name = $("#name").val();
        var protein = $("#protein").val();
        var carbs = $("#carbs").val();
        var fat = $("#fat").val();
        var data = {
            name: name,
            protein: protein,
            carbs: carbs,
            fat: fat

        };

        console.log(data);
        var contextPath = window.location.protocol + '//' + window.location.host;
        var url = contextPath + '/product/add';

        $.post({
            url: url,
            data: JSON.stringify(data),
            contentType: 'application/json'
        }).done(function (data) {
            console.log(data);
        }).fail(function (error) {
                console.log(error.responseText)
            });
    })
});