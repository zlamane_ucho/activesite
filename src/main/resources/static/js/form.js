$(document).ready(function () {
    var form = $("#dataForm"); //handler na formularz

    $("#submitToDB").click(function (event) {
        event.preventDefault();
        var login = $("#login").val();
        var password = $("#password").val();
        var email = $("#email").val();
        var data = {
            login: login,
            password: password,
            email: email
        };

        console.log(data);

        var contextPath = window.location.protocol + '//' + window.location.host;
        var url = contextPath + '/database/add';

        $.post({
            url: url,
            data: JSON.stringify(data),
            contentType: 'application/json'
        }).done(function (data) {
            $('#test').html(data);
        }).fail(function (error) {
                console.log(error.responseText)
            });
    })
});